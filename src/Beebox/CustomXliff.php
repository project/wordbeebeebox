<?php

namespace Drupal\tmgmt_wordbee\Beebox;

use Drupal\tmgmt_file\Plugin\tmgmt_file\Format\Xliff;
use Drupal\tmgmt\JobInterface;
use Drupal\tmgmt\Entity\Job;

class CustomXliff extends Xliff
{

    /**
     * Same as Xliff::addTransUnit() exept it leaves the target element empty
     *
     * @param string $key
     * @param array $element
     */
    protected function addTransUnit($key, $element, JobInterface $job)
    {
        $key_array = \Drupal::service('tmgmt.data')->ensureArrayKey($key);

        $this->startElement('trans-unit');
        $this->writeAttribute('id', $key);
        $this->writeAttribute('resname', $key);

        $this->startElement('source');
        $this->writeAttribute('xml:lang', $this->job->getTranslator()->mapToRemoteLanguage($this->job->source_language));

        if ($job->getSetting('xliff_processing')) {
            $this->writeRaw($this->processForExport($element['#text'], $key_array));
        } else {
            $this->text($element['#text']);
        }

        $this->endElement();
    }
}
